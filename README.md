# Hyperion
Simple command-line interface for controlling a strip of WS2812B LEDs via a bitmap on a Raspberry Pi.
## Installation
Ensure you've added `~/.cargo/bin` to your `PATH`, then execute the following in the root of this repository:
```
cargo install --path .
```
You may also install without manually cloning the repository:
```
cargo install --git https://gitlab.com/628/hyperion.git --branch stable
```
## Setup
This program uses GPIO 18 on the Raspberry Pi for the data output, so you must wire that to the `DIN` of the LED.  

Some strips may not support the 3.3V that the Pi uses for its GPIO pins, in which case you must use a level shifter or 
something equivalent.  

Additional information can be found on the [WS2812B datasheet](https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf).
## Usage
This program utilizes a provided bitmap and frame rate in order to display an animation.  

The amount of LEDs in the system is inferred by the width of the bitmap image and each row corresponds to a single frame.
At a framerate of 100, for example, each frame lasts 0.01s; so a bitmap image with a height of 500 at that 
framerate would take approximately 5s to complete.  

A sample bitmap is provided at `samples/bitmap.png` with a width of 5.
```
Hyperion 1.0.0

USAGE:
    hyperion [FLAGS] [OPTIONS]

FLAGS:
    -c               Clears the strip after the animation is complete.
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -b, --bitmap <bitmap>    Path to the bitmap file.
    -f, --fps <fps>          FPS of the animation.
    -l, --loop <loop>        Amount of times to loop (-1 for infinite loop). [default: 1]
```
## Known Issues
The frame rate of the animation is limited by a multitude of factors, some being out of the user's control.  

After extensive testing, ~150fps seems to be the 
upper limit for the Raspberry Pi Zero W with an error of about ±200μs. If accuracy is important 
(e.g. with audio synchronization), it's recommended that you do your own testing to find a maximum frame rate with an
acceptable error.  

You should also set the `NI` value of the program to the highest priority to minimize error.