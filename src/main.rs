extern crate image;

use std::path::Path;
use std::time::{Duration, Instant};

use clap::{App, Arg};
use image::{DynamicImage, GenericImageView};
use rs_ws281x::{ChannelBuilder, ControllerBuilder, StripType};

fn bitmap_to_animation_steps(bitmap: DynamicImage) -> Vec<Vec<[u8; 4]>> {
    let bitmap_width = bitmap.dimensions().0;
    let bitmap_height = bitmap.dimensions().1;

    let mut animation_steps: Vec<Vec<[u8; 4]>> = Vec::with_capacity(bitmap_width as usize);

    for y in 0..bitmap_height {
        let mut step: Vec<[u8; 4]> = Vec::with_capacity(bitmap_width as usize);

        for x in 0..bitmap_width {
            let p = bitmap.get_pixel(x, y);
            step.push([p[0], p[1], p[2], 0])
        }

        animation_steps.push(step)
    }

    return animation_steps;
}

struct HyperionArgs {
    bitmap: DynamicImage,
    fps: u32,
    loops: i32,
    clear: bool,
}

fn get_args() -> HyperionArgs {
    let matches = App::new("Hyperion")
        .version("1.1.0")
        .arg(
            Arg::new("bitmap")
                .about("Path to the bitmap file.")
                .long("bitmap")
                .short('b')
                .takes_value(true)
        )
        .arg(
            Arg::new("fps")
                .about("FPS of the animation.")
                .long("fps")
                .short('f')
                .takes_value(true)
        )
        .arg(
            Arg::new("loop")
                .about("Amount of times to loop (-1 for infinite loop).")
                .long("loop")
                .short('l')
                .takes_value(true)
                .default_value("1")
        )
        .arg(
            Arg::new("clear")
                .about("Clears the strip after the animation is complete.")
                .short('c')
                .takes_value(false)
        )
        .get_matches();

    HyperionArgs {
        bitmap: {
            image::open(&Path::new(
                &matches.value_of("bitmap")
                    .expect("Error getting the bitmap path arg!!"))
            ).expect("There was a problem opening the provided bitmap.")
        },

        fps: matches.value_of("fps")
            .expect("Error getting the fps arg!")
            .parse::<u32>()
            .expect("Error converting FPS to a number!"),

        loops: matches.value_of("loop")
            .expect("Error getting the loops arg!")
            .parse::<i32>()
            .expect("Error converting the loops to a number!"),

        clear: matches.is_present("clear"),
    }
}

fn main() {
    let args = get_args();

    let mut controller = ControllerBuilder::new()
        .freq(800_000)
        .dma(10)
        .channel(
            0,
            ChannelBuilder::new()
                .pin(18)
                .count(args.bitmap.dimensions().0 as i32)
                .invert(false)
                .brightness(255)
                .strip_type(StripType::Ws2811Gbr)
                .build(),
        ).build().expect("Error initializing the controller!");

    let animation_steps: Vec<Vec<[u8; 4]>> = bitmap_to_animation_steps(args.bitmap);

    let spin_sleeper = spin_sleep::SpinSleeper::new(100_000);

    let inf_loop = args.loops == -1;
    let mut c_loop = 0;

    let frame_wait = (((1.0 / (args.fps as f64))
        * 1e+6) as f64).floor() as i64;

    while inf_loop || c_loop < args.loops {
        for step in &animation_steps {
            let now = Instant::now();

            for (l, s)
            in controller.leds_mut(0).iter_mut().zip(step) {
                *l = *s;
            }

            controller.render().expect("Rendering error!");
            let elapsed_us = now.elapsed().as_micros() as i64;
            let adjusted_frame_wait = (frame_wait - elapsed_us).max(0) as u64;

            spin_sleeper.sleep(Duration::from_micros(adjusted_frame_wait))
        }

        c_loop += 1;
    }

    if args.clear {
        controller.leds_mut(0).iter_mut().for_each(
            |e| *e = [0, 0, 0, 0]
        );

        controller.render().expect("Rendering error!");
    }
}